libmath-prime-util-perl (0.68-1) unstable; urgency=medium

  From upstream Changes:

    [API Changes]

    - forcomb with one argument iterates over the power set, so k=0..n
      instead of k=n.  The previous behavior was undocumented.  The new
      behavior matches Pari/GP (forsubset) and Perl6 (combinations).

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Nov 2017 18:31:43 +0100

libmath-prime-util-perl (0.65-1) unstable; urgency=medium

  From upstream Changes:

    [API Changes]

    - Config options irand and primeinc are deprecated.  They will carp if set.

    - The 'irand' config option is removed, as we now use our own CSPRNG.
      It can be seeded with csrand() or srand().  The latter is not exported.

    - The 'primeinc' config option is deprecated and will go away soon.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Aug 2017 17:50:13 -0400

libmath-prime-util-perl (0.58-1) unstable; urgency=medium

  From 0.58 upstream Changes:

    [API Changes]

    - prev_prime($n) where $n <= 2 now returns undef instead of 0.  This
      may enable catching range errors, and is technically more correct.

    - nth_prime(0) now returns undef instead of 0.  This should help catch
      cases where the base wasn't understood.  The change is similar for
      all the nth_* functions (e.g. nth_twin_prime).

    - sumdigits(n,base) will interpret n as a number in the given base,
      rather than the Pari/GP method of converting decimal n to that base
      then summing.  This allows sumdigits to easily sum hex strings.
      The old behavior is easily done with vecsum(todigits(n, base)).

    - binary() was not intended to be released (todigits and todigitstring
      are supersets), but the documentation got left in.  Remove docs.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 22 May 2016 11:03:56 +0200

libmath-prime-util-perl (0.46-1) unstable; urgency=medium

  From 0.46 upstream Changes:

    [API Changes]

    - is_pseudoprime has the same signature as is_strong_pseudoprime now.
      This means it requires one or more bases and has no default base.
      The documentation had never mentioned the default, so this should
      have little impact, and the common signature makes more sense.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Oct 2014 19:47:22 +0200

libmath-prime-util-perl (0.36-1) unstable; urgency=medium

  From 0.36 upstream Changes:

    [API Changes]

    - factor behavior for 0 and 1 more consistent.  The results for
      factor, factor_exp, divisors, and divisor_sum now match Pari,
      and the omega(1)/Omega(1) exception is removed.

      Thanks to Hugo van der Sanden for bringing this up.

    - all_factors changed to divisors.  The old name still remains aliased.

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 14 Jan 2014 21:29:44 +0100

libmath-prime-util-perl (0.35-1) unstable; urgency=medium

  From upstream Changes:

  0.35  2013-12-08

    [API Changes]

    - We now use Math::BigInt in the module rather than dynamically loading
      it, and will switch to BigInts as needed.  The most noticeable effect
      of this is that next_prime() / prev_prime() will switch between BigInt
      and native int at the boundary without regard to the input type or
      whether bigint is in effect, and next_prime will never return 0.
      Additionally, all functions will convert large decimal number strings
      to BigInts if needed.

      $pref = primes("1000000000000000000000", "1000000000000000000999");
      is_prime("882249208105452588824618008529");
      $a = euler_phi("801294088771394680000412");

  0.33  2013-11-18

    [API Changes]

    - all_factors now includes 1 and n, making it identical to Pari's
      divisors(n) function, but no longer identical to Math::Factor::XS's
      factors(n) function.  This change allows consistency between
      divisor_sum(n,0) and scalar all_factors(n).

  0.30  2013-08-06

    [API Changes]
      - Primality proofs now use the new "MPU Certificate" format, which is
        text rather than a nested Perl data structure.  This is much better
        for external interaction, especially with non-Perl tools.  It is
        not quite as convenient for all-Perl manipulation.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Dec 2013 22:22:14 +0100
